import internal_external_multi
import json
import os

##Current ongoing simulation has problem that IhIt extention belongs to HpTp
def simulate(iter):
    relative = "../"
    path=os.path.abspath(relative)
    print("iter:",iter)
    flName =path+"/Experiments/2_IhIt_"+str(iter)+".json"
    #fl = open(flName,'wb+')
    internal_external_multi.isIhIt_NN = True
    internal_external_multi.isPher_Lev_NN=False
    internal_external_multi.displaySize=0
    internal_external_multi.time = 200000
    data=internal_external_multi.run(32.0)
    #pickle.dump(data,fl)
    #fl.close()
    with open(flName, 'w') as fp:
        json.dump(data, fp,sort_keys=True)

def simulate2(iter):
    relative = "../"
    path=os.path.abspath(relative)
    print("iter:",iter)
    flName =path+"/Experiments/2_HpTp_Ih_It_"+str(iter)+".json"
    #fl = open(flName,'wb+')
    internal_external_multi.isIhIt_NN = True
    internal_external_multi.isPher_Lev_NN=True
    internal_external_multi.displaySize=0
    internal_external_multi.time = 200000
    data=internal_external_multi.run(32.0)
    #pickle.dump(data,fl)
    #fl.close()
    with open(flName, 'w') as fp:
        json.dump(data, fp,sort_keys=True)


def simulate3(iter):
    relative = "../"
    path=os.path.abspath(relative)
    print("iter:",iter)
    flName =path+"/Experiments/2_HpTp_"+str(iter)+".json"
    #fl = open(flName,'wb+')
    internal_external_multi.isIhIt_NN = False
    internal_external_multi.isPher_Lev_NN=True
    internal_external_multi.displaySize=0
    internal_external_multi.time = 200000
    data=internal_external_multi.run(32.0)
    #pickle.dump(data,fl)
    #fl.close()
    with open(flName, 'w') as fp:
        json.dump(data, fp,sort_keys=True)


if __name__ == "__main__":
    lst=list(range(3))
    internal_external_multi.multiProcessSimulation(2,lst,simulate)